import matplotlib.pyplot as plt
import pandas as pd
import json
import math
import numpy
from dateutil import parser
from datetime import timedelta

MIN_DURATION_THRESHOLD_MS = 500
EVENT_START = parser.parse('2019-02-07T06:00:00Z')
EVENT_END = parser.parse('2019-02-07T08:00:00Z')


def parse_multiple_logs(files):
    log_data = {}

    for key, filename in files.items():
        data = parse_json_log(filename)
        log_data[key] = sorted(data, key=lambda k: k['start_ts'])

    return log_data


def parse_json_log(filename):
    print("Parsing %s" % filename)
    log_data = []

    with open(filename) as f:
        for line in f:
            data = json.loads(line)
            end_timestamp = parser.parse(data['time'])
            duration = float(data['duration'])
            start_time = end_timestamp - timedelta(seconds=duration)

            if duration >= MIN_DURATION_THRESHOLD_MS and start_time >= EVENT_START and start_time <= EVENT_END:
                log_data.append({'start_ts': start_time, 'duration': duration})

    return log_data


log_data = parse_multiple_logs({'api': 'api_json.log.1', 'prod': 'production_json.log.1'})
start_ts = numpy.min([data[0]['start_ts'] - timedelta(seconds=1) for key, data in log_data.items()])
end_ts = numpy.max([data[-1]['start_ts'] + timedelta(milliseconds=data[-1]['duration'] + 1000) for key, data in log_data.items()])

print("start ts %s" % start_ts.replace(microsecond=0))
date_index = pd.date_range(start=start_ts.replace(microsecond=0), end=end_ts.replace(microsecond=0), freq='s').round('1s')
active_count_keys = [['%s_active_count' % x for x in log_data.keys()]]
df = pd.DataFrame(index=date_index, columns=active_count_keys).fillna(0)

for key, data_for_type in log_data.items():
    for data in data_for_type:
        for x in range(math.ceil(data['duration'] / 1000.0)):
            current_ts = data['start_ts'] + timedelta(seconds=x)
            active_time = pd.Timestamp(current_ts).round('1s')
            col_name = "%s_active_count" % key
            value = df.loc[active_time][col_name].values[0]
            df.set_value(active_time, col_name, value + 1)

fig, ax = plt.subplots()
plt.subplot(2, 1, 1)
df.plot(ax=plt.gca(), figsize=(10, 5), grid=True, stacked=True, y=active_count_keys, colors=['red', 'blue'], legend=False, title='Total number of requests taking > 500 ms')

plt.subplot(2, 1, 2)
ax = plt.gca()
df.plot(ax=ax, figsize=(10, 5), grid=True, stacked=False, y=active_count_keys, colors=['red', 'blue'], title='Number of requests taking > 500 ms by type')
ax.legend(['api', 'ui'], loc='center left', bbox_to_anchor=(1, 0.5))
plt.tight_layout()

plt.savefig("active-conns.png")
